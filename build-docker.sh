mvn clean package -DskipTests
docker build -f src/main/docker/Dockerfile.${QUARKUS_MODE:-jvm} \
  -t registry.gitlab.com/archertech-lab/quarkus-grpc .
docker push registry.gitlab.com/archertech-lab/quarkus-grpc