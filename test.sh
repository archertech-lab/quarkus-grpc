SERVER=localhost:9000

grpcurl -import-path ./src/main/proto -proto hello.proto list
#exit

grpcurl -plaintext $SERVER list hello.HelloGrpc
#exit

# no TLS
grpcurl -import-path ./src/main/proto -proto hello.proto \
 -d '{"name":"Jane Doe"}' -plaintext $SERVER hello.HelloGrpc.SayHello